﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Paynetics.Services.Interfaces;

using Microsoft.AspNetCore.Authorization;

using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paynetics.Web.Controllers
{
    public class XmlController : Controller
    {
        private readonly ISerializerService _serializerService;
        private readonly ITransactionService _transactionService;
        private readonly IExportService _exportService;

        public XmlController(ISerializerService serializerService, ITransactionService transactionService, IExportService exportService)
        {
            this._serializerService = serializerService;
            this._transactionService = transactionService;
            this._exportService = exportService;
        }

        [HttpGet]
        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Upload(IFormFile file)
        {
            if(file != null)
            {
                var operation = this._serializerService.Serializer(file);

                if (operation.Transactions.Count() != 0)
                {
                    await _transactionService.CreateTransactionsAsync(operation.Transactions);
                }
            }

            return RedirectToAction("Index", "Transaction");
        }

        [HttpPost]
        public async Task<IActionResult> ExportCsv(DateTime date)
        {
            var transactions = await this._transactionService.GetAllTransactionsAsync(date);

            var file = new StringBuilder();

            if (transactions.Count() != 0)
            {
                file = this._exportService.Export(transactions);
            }

            return File(Encoding.UTF8.GetBytes(file.ToString()), "application/vnd.ms-excel", "Transactions.csv");
        }
    }
}