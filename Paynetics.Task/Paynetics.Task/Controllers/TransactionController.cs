﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Paynetics.Models.Models;
using Paynetics.Services.Interfaces;
using System.Threading.Tasks;

namespace Paynetics.Web.Controllers
{
    public class TransactionController : Controller
    {
        private readonly ITransactionService _transactionService;

        public TransactionController(ITransactionService transactionService)
        {
            this._transactionService = transactionService;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Index()
        {
            var allTransactions = await this._transactionService.GetAllTransactionsAsync();

            return View(allTransactions);
        }

        [HttpGet]
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Create(Transaction transaction)
        {
            await this._transactionService.CreateTransactionAsync(transaction);

            return RedirectToAction("Index", "Transaction");
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Delete(int transactionId)
        {
            await _transactionService.DeleteTransactionAsync(transactionId);

            return RedirectToAction("Index", "Transaction");
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Details(int transactionId)
        {
            var currentTransaction = await _transactionService.GetTransactionAsync(transactionId);

            return View(currentTransaction);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Update(int transactionId)
        {
            var currentTransaction = await _transactionService.GetTransactionAsync(transactionId);

            return View(currentTransaction);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Update(Transaction transaction)
        {
            await this._transactionService.UpdateTransactionAsync(transaction);

            return RedirectToAction("Index", "Transaction");
        }
    }
}
