﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Paynetics.Models.Models;
using Paynetics.Services.Interfaces;
using System.Threading.Tasks;

namespace Paynetics.Web.Controllers
{
    public class MerchantController : Controller
    {
        private readonly IMerchantService _merchantService;

        public MerchantController(IMerchantService merchantService)
        {
            this._merchantService = merchantService;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Index()
        {
            var allMerchants = await this._merchantService.GetAllMerchantsAsync();

            return View(allMerchants);
        }

        [HttpGet]
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Create(Merchant merchant)
        {
            await this._merchantService.CreateMerchantAsync(merchant);

            return RedirectToAction("Index", "Merchant");
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Delete(int merchantId)
        {
            await _merchantService.DeleteMerchantAsync(merchantId);

            return RedirectToAction("Index", "Merchant");
        }
        
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Details(string name)
        {
            var currentMerchant = await _merchantService.GetMerchantAsync(name);

            return View(currentMerchant);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Update(string name)
        {
            var currentMerchant = await _merchantService.GetMerchantAsync(name);

            return View(currentMerchant);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Update(Merchant merchant)
        {
            await this._merchantService.UpdateMerchantAsync(merchant);

            return RedirectToAction("Index", "Merchant");
        }
    }
}