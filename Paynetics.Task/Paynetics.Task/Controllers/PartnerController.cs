﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

using Paynetics.Models.Models;
using Paynetics.Services.Interfaces;

using System.Threading.Tasks;

namespace Paynetics.Web.Controllers
{
    public class PartnerController : Controller
    {
        private readonly IPartnerService _partnerService;

        public PartnerController(IPartnerService partnerService)
        {
            this._partnerService = partnerService;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Index()
        {
            var allPartners = await this._partnerService.GetAllPartnersAsync();

            return View(allPartners);
        }

        [HttpGet]
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Create(Partner partner)
        {
            await this._partnerService.CreatePartnerAsync(partner);

            return RedirectToAction("Index", "Partner");
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Delete(int partnerId)
        {
            await _partnerService.DeletePartnerAsync(partnerId);

            return RedirectToAction("Index", "Partner");
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Details(int partnerId)
        {
            var currentPartner = await _partnerService.GetPartnerAsync(partnerId);

            return View(currentPartner);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Update(int partnerId)
        {
            var currentPartner = await _partnerService.GetPartnerAsync(partnerId);

            return View(currentPartner);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Update(Partner partner)
        {
            await this._partnerService.UpdatePartnerAsync(partner);

            return RedirectToAction("Index", "Partner");
        }
    }
}
