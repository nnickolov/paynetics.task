﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Paynetics.Database.Migrations
{
    public partial class RemovingPropertiesFromTransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Amounts_Transaction_TransactionId",
                table: "Amounts");

            migrationBuilder.DropForeignKey(
                name: "FK_Beneficiaries_Transaction_TransactionId",
                table: "Beneficiaries");

            migrationBuilder.DropForeignKey(
                name: "FK_Debtors_Transaction_TransactionId",
                table: "Debtors");

            migrationBuilder.DropIndex(
                name: "IX_Debtors_TransactionId",
                table: "Debtors");

            migrationBuilder.DropIndex(
                name: "IX_Beneficiaries_TransactionId",
                table: "Beneficiaries");

            migrationBuilder.DropIndex(
                name: "IX_Amounts_TransactionId",
                table: "Amounts");

            migrationBuilder.DropColumn(
                name: "TransactionId",
                table: "Debtors");

            migrationBuilder.DropColumn(
                name: "TransactionId",
                table: "Beneficiaries");

            migrationBuilder.DropColumn(
                name: "TransactionId",
                table: "Amounts");

            migrationBuilder.AddColumn<int>(
                name: "AmountId",
                table: "Transaction",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BeneficiaryId",
                table: "Transaction",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DebtorId",
                table: "Transaction",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Transaction_AmountId",
                table: "Transaction",
                column: "AmountId",
                unique: true,
                filter: "[AmountId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Transaction_BeneficiaryId",
                table: "Transaction",
                column: "BeneficiaryId",
                unique: true,
                filter: "[BeneficiaryId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Transaction_DebtorId",
                table: "Transaction",
                column: "DebtorId",
                unique: true,
                filter: "[DebtorId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Transaction_Amounts_AmountId",
                table: "Transaction",
                column: "AmountId",
                principalTable: "Amounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Transaction_Beneficiaries_BeneficiaryId",
                table: "Transaction",
                column: "BeneficiaryId",
                principalTable: "Beneficiaries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Transaction_Debtors_DebtorId",
                table: "Transaction",
                column: "DebtorId",
                principalTable: "Debtors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transaction_Amounts_AmountId",
                table: "Transaction");

            migrationBuilder.DropForeignKey(
                name: "FK_Transaction_Beneficiaries_BeneficiaryId",
                table: "Transaction");

            migrationBuilder.DropForeignKey(
                name: "FK_Transaction_Debtors_DebtorId",
                table: "Transaction");

            migrationBuilder.DropIndex(
                name: "IX_Transaction_AmountId",
                table: "Transaction");

            migrationBuilder.DropIndex(
                name: "IX_Transaction_BeneficiaryId",
                table: "Transaction");

            migrationBuilder.DropIndex(
                name: "IX_Transaction_DebtorId",
                table: "Transaction");

            migrationBuilder.DropColumn(
                name: "AmountId",
                table: "Transaction");

            migrationBuilder.DropColumn(
                name: "BeneficiaryId",
                table: "Transaction");

            migrationBuilder.DropColumn(
                name: "DebtorId",
                table: "Transaction");

            migrationBuilder.AddColumn<int>(
                name: "TransactionId",
                table: "Debtors",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TransactionId",
                table: "Beneficiaries",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TransactionId",
                table: "Amounts",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Debtors_TransactionId",
                table: "Debtors",
                column: "TransactionId");

            migrationBuilder.CreateIndex(
                name: "IX_Beneficiaries_TransactionId",
                table: "Beneficiaries",
                column: "TransactionId");

            migrationBuilder.CreateIndex(
                name: "IX_Amounts_TransactionId",
                table: "Amounts",
                column: "TransactionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Amounts_Transaction_TransactionId",
                table: "Amounts",
                column: "TransactionId",
                principalTable: "Transaction",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Beneficiaries_Transaction_TransactionId",
                table: "Beneficiaries",
                column: "TransactionId",
                principalTable: "Transaction",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Debtors_Transaction_TransactionId",
                table: "Debtors",
                column: "TransactionId",
                principalTable: "Transaction",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
