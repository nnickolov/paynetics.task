﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Paynetics.Models.Models;
using System.Threading.Tasks;
using System.Threading;
using System;
using System.Linq;
using Paynetics.Models.Interfaces;

namespace Paynetics.Database
{
    public class PayneticsDbContext : IdentityDbContext
    {
        public PayneticsDbContext(DbContextOptions<PayneticsDbContext> options)
            : base(options)
        {
        }

        public DbSet<Partner> Partners { get; set; }

        public DbSet<Merchant> Merchants { get; set; }

        public DbSet<Transaction> Transaction { get; set; }

        public DbSet<Amount> Amounts { get; set; }

        public DbSet<Debtor> Debtors { get; set; }

        public DbSet<Beneficiary> Beneficiaries { get; set; }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            this.ApplyBaseRules();
            return base.SaveChangesAsync();
        }

        private void ApplyBaseRules()
        {
            foreach (var entityToAdd in this.ChangeTracker.Entries()
                .Where(x => x.Entity is IHasCreationDate && (x.State == EntityState.Added)))
            {
                var currentEntity = (IHasCreationDate)entityToAdd.Entity;
                if (entityToAdd.State == EntityState.Added && currentEntity.CreateDate == default(DateTime))
                {
                    currentEntity.CreateDate = DateTime.UtcNow;
                }
            }
        }
    }
}
