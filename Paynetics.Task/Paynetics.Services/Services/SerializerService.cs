﻿using Microsoft.AspNetCore.Http;

using Paynetics.Models.Models;
using Paynetics.Services.Interfaces;

using System.Xml;
using System.Xml.Serialization;

namespace Paynetics.Services.Services
{
    public class SerializerService : ISerializerService
    {
        public Operation Serializer(IFormFile file)
        {
            var currentFile = file.OpenReadStream();

            currentFile.Flush();
            currentFile.Position = 0;

            var reader = XmlReader.Create(currentFile);

            XmlSerializer serializer = new XmlSerializer(typeof(Operation));
            Operation operation = (Operation)serializer.Deserialize(reader);

            return operation;
        }
    }
}
