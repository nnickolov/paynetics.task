﻿using Paynetics.Database;
using Paynetics.Models.Models;
using Paynetics.Services.Interfaces;

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.EntityFrameworkCore;

namespace Paynetics.Services.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly PayneticsDbContext _payneticsDbContext;

        public TransactionService(PayneticsDbContext payneticsDbContext)
        {
            this._payneticsDbContext = payneticsDbContext;
        }

        #region Create

        public async Task CreateTransactionAsync(Transaction transaction)
        {
            await this._payneticsDbContext.Transaction.AddAsync(transaction);

            await this._payneticsDbContext.SaveChangesAsync();
        }

        public async Task CreateTransactionsAsync(IEnumerable<Transaction> transactions)
        {
            await this._payneticsDbContext.Transaction.AddRangeAsync(transactions);

            await this._payneticsDbContext.SaveChangesAsync();
        }

        #endregion

        #region Read

        public async Task<Transaction> GetTransactionAsync(int id)
        {
            var transaction = await _payneticsDbContext.Transaction
                .Where(x => x.Id == id && x.IsDeleted == false)
                .FirstOrDefaultAsync();

            return transaction;
        }

        public async Task<ICollection<Transaction>> GetAllTransactionsAsync()
        {
            var transactions = await _payneticsDbContext.Transaction
                .Where(x => x.IsDeleted == false)
                .Include(x => x.Amount)
                .Include(y => y.Debtor)
                .Include(r => r.Beneficiary)
                .ToListAsync();

            return transactions;
        }

        public async Task<IEnumerable<Transaction>> GetAllTransactionsAsync(DateTime dateTime)
        {
            var transactions = await _payneticsDbContext.Transaction
               .Where(x => x.CreateDate.Year == dateTime.Year && x.CreateDate.Month == dateTime.Month
                      && x.CreateDate.Day == dateTime.Day && x.IsDeleted == false)
               .Include(x => x.Amount)
               .Include(y => y.Debtor)
               .Include(r => r.Beneficiary)
               .ToListAsync();

            return transactions;
        }

        #endregion

        #region Update

        public async Task<Transaction> UpdateTransactionAsync(Transaction transaction)
        {
            var currentTransaction = await this._payneticsDbContext.Transaction
                     .FirstOrDefaultAsync(x => x.Id == transaction.Id);

            if (string.IsNullOrEmpty(transaction.ExternalId))
            {
                currentTransaction.ExternalId = transaction.ExternalId;
            }

            currentTransaction.Amount = transaction.Amount;

            if (transaction.Status != null)
            {
                currentTransaction.Status = transaction.Status;
            }

            currentTransaction.Debtor = transaction.Debtor;

            currentTransaction.Beneficiary = transaction.Beneficiary;

            await _payneticsDbContext.SaveChangesAsync();

            return currentTransaction;
        }

        #endregion

        #region Delete

        public async Task<Transaction> DeleteTransactionAsync(int id)
        {
            var transaction = await this._payneticsDbContext.Transaction
                .FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);

            transaction.IsDeleted = true;

            await _payneticsDbContext.SaveChangesAsync();

            return transaction;
        }

        #endregion
    }
}
