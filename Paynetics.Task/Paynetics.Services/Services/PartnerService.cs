﻿using Paynetics.Database;
using Paynetics.Models.Models;
using Paynetics.Services.Interfaces;

using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Paynetics.Services.Services
{
    public class PartnerService : IPartnerService
    {
        private readonly PayneticsDbContext _payneticsDbContext;

        public PartnerService(PayneticsDbContext payneticsDbContext)
        {
            this._payneticsDbContext = payneticsDbContext;
        }

        #region Create

        public async Task CreatePartnerAsync(Partner partner)
        {
            await this._payneticsDbContext.Partners.AddAsync(partner);

            await this._payneticsDbContext.SaveChangesAsync();
        }

        #endregion 

        #region Read

        public async Task<Partner> GetPartnerAsync(int id)
        {
            var partners = await _payneticsDbContext.Partners
                 .Where(x => x.Id == id && x.IsDeleted == false)
                 .FirstOrDefaultAsync();

            return partners;
        }

        public async Task<ICollection<Partner>> GetAllPartnersAsync()
        {
            var partners = await _payneticsDbContext.Partners
                .Where(x => x.IsDeleted == false)
                .ToListAsync();

            return partners;
        }

        #endregion

        #region Update

        public async Task<Partner> UpdatePartnerAsync(Partner partner)
        {
            var currentPartner = await this._payneticsDbContext.Partners
                    .FirstOrDefaultAsync(x => x.Id == partner.Id);


            currentPartner.Name = partner.Name ?? currentPartner.Name;

            if (partner.Merchants != null)
            {
                currentPartner.Merchants = partner.Merchants;
            }

            await _payneticsDbContext.SaveChangesAsync();

            return currentPartner;
        }

        #endregion

        #region Delete

        public async Task<Partner> DeletePartnerAsync(int id)
        {
            var partner = await this._payneticsDbContext.Partners
               .FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);

            partner.IsDeleted = true;

            await _payneticsDbContext.SaveChangesAsync();

            return partner;
        }

        #endregion 
    }
}
