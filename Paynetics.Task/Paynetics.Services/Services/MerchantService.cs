﻿using Paynetics.Database;
using Paynetics.Models.Models;
using Paynetics.Services.Interfaces;

using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;

namespace Paynetics.Services.Services
{
    public class MerchantService : IMerchantService
    {
        private readonly PayneticsDbContext _payneticsDbContext;

        public MerchantService(PayneticsDbContext payneticsDbContext)
        {
            this._payneticsDbContext = payneticsDbContext;
        }

        #region Create

        public async Task CreateMerchantAsync(Merchant merchant)
        {
            await this._payneticsDbContext.Merchants.AddAsync(merchant);

            await this._payneticsDbContext.SaveChangesAsync();
        }

        #endregion

        #region Read

        public async Task<Merchant> GetMerchantAsync(string name)
        {
            var merchant = await _payneticsDbContext.Merchants
                .Where(x => x.Name == name && x.IsDeleted == false)
                .FirstOrDefaultAsync();

            return merchant;
        }

        public async Task<ICollection<Merchant>> GetAllMerchantsAsync()
        {
            var merchants = await _payneticsDbContext.Merchants
               .Where(x => x.IsDeleted == false)
               .ToListAsync();

            return merchants;
        }

        #endregion 

        #region Update

        public async Task<Merchant> UpdateMerchantAsync(Merchant merchant)
        {
            var currentMerchant = await this._payneticsDbContext.Merchants
                     .FirstOrDefaultAsync(x => x.Id == merchant.Id);


            currentMerchant.Name = merchant.Name ?? currentMerchant.Name;

            if (merchant.BoardingDate != null)
            {
                currentMerchant.BoardingDate = merchant.BoardingDate;
            }

            currentMerchant.Url = merchant.Url ?? currentMerchant.Url;

            currentMerchant.Country = merchant.Country ?? currentMerchant.Country;

            currentMerchant.FirstAddress = merchant.FirstAddress ?? currentMerchant.FirstAddress;

            currentMerchant.SecondAddress = merchant.SecondAddress ?? currentMerchant.SecondAddress;

            if (merchant.Transactions != null)
            {
                currentMerchant.Transactions = merchant.Transactions;
            }

            await _payneticsDbContext.SaveChangesAsync();

            return currentMerchant;
        }

        #endregion 

        #region Delete

        public async Task<Merchant> DeleteMerchantAsync(int id)
        {
            var merchant = await this._payneticsDbContext.Merchants
              .FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);

            merchant.IsDeleted = true;

            await _payneticsDbContext.SaveChangesAsync();

            return merchant;
        }

        #endregion 
    }
}
