﻿using System.Collections.Generic;

using Paynetics.Services.Interfaces;
using System.Text;
using System.Linq;
using Paynetics.Models.Models;

namespace Paynetics.Services.Services
{
    public class ExportService : IExportService
    {
        public StringBuilder Export(IEnumerable<Transaction> transactions)
        {
            var sb = new StringBuilder();
            var columns = new List<object>();
            var transactionType = transactions.FirstOrDefault().GetType();
            var transactionProperties = transactionType.GetProperties();
            var propertyNames = transactionProperties.Select(x => x.Name).ToArray();

            columns.Insert(0, propertyNames);

            foreach (var item in columns)
            {
                var propertyData = (string[])item;

                sb.AppendLine(string.Join(",", propertyData));

                foreach (var currentTransaction in transactions)
                {
                    foreach (var propData in propertyData)
                    {
                        var currentProperty = transactionProperties.FirstOrDefault(x => x.Name == propData);

                        var propValue = string.Empty;

                        if (currentProperty.PropertyType.Name == nameof(Amount))
                        {
                            propValue = (currentProperty?.GetValue(currentTransaction) as Amount)?.Value.ToString();
                        }
                        else if (currentProperty.PropertyType.Name == nameof(Debtor))
                        {
                            propValue = (currentProperty?.GetValue(currentTransaction) as Debtor)?.BankName?.ToString();
                        }
                        else if (currentProperty.PropertyType.Name == nameof(Beneficiary))
                        {
                            propValue = (currentProperty?.GetValue(currentTransaction) as Beneficiary)?.BankName?.ToString();
                        }
                        else if (currentProperty.PropertyType.Name == nameof(Merchant))
                        {
                            propValue = (currentProperty?.GetValue(currentTransaction) as Merchant)?.Name?.ToString();
                        }
                        else
                        {
                            propValue = currentProperty?.GetValue(currentTransaction)?.ToString();
                        }

                        sb.Append(propValue + ",");
                    }
                    sb.Append("\r\n");
                }
            }

            return sb;
        }
    }
}
