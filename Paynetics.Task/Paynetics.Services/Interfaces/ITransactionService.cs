﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

using Paynetics.Models.Models;

namespace Paynetics.Services.Interfaces
{
    public interface ITransactionService
    {
        #region Create

        Task CreateTransactionAsync(Transaction transaction);
        Task CreateTransactionsAsync(IEnumerable<Transaction> transactions);

        #endregion

        #region Read

        Task<Transaction> GetTransactionAsync(int id);

        Task<ICollection<Transaction>> GetAllTransactionsAsync();

        Task<IEnumerable<Transaction>> GetAllTransactionsAsync(DateTime dateTime);

        #endregion

        #region Update

        Task<Transaction> UpdateTransactionAsync(Transaction transaction);

        #endregion

        #region Delete

        Task<Transaction> DeleteTransactionAsync(int id);

        #endregion
    }
}
