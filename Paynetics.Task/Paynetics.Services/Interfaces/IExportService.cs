﻿using Paynetics.Models.Models;
using System.Collections.Generic;
using System.Text;

namespace Paynetics.Services.Interfaces
{
    public interface IExportService
    {
        StringBuilder Export(IEnumerable<Transaction> transactions);
    }
}
