﻿using System.Threading.Tasks;
using System.Collections.Generic;

using Paynetics.Models.Models;

namespace Paynetics.Services.Interfaces
{
    public interface IMerchantService
    {
        #region Create

        Task CreateMerchantAsync(Merchant merchant);

        #endregion

        #region Read

        Task<Merchant> GetMerchantAsync(string name);

        Task<ICollection<Merchant>> GetAllMerchantsAsync();

        #endregion

        #region Update

        Task<Merchant> UpdateMerchantAsync(Merchant merchant);

        #endregion

        #region Delete

        Task<Merchant> DeleteMerchantAsync(int id);

        #endregion
    }
}
