﻿using Microsoft.AspNetCore.Http;
using Paynetics.Models.Models;

namespace Paynetics.Services.Interfaces
{
    public interface ISerializerService
    {
        Operation Serializer(IFormFile file);
    }
}
