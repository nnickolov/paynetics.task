﻿using System.Threading.Tasks;
using System.Collections.Generic;

using Paynetics.Models.Models;

namespace Paynetics.Services.Interfaces
{
    public interface IPartnerService
    {
        #region Create

        Task CreatePartnerAsync(Partner partner);

        #endregion

        #region Read

        Task<Partner> GetPartnerAsync(int id);

        Task<ICollection<Partner>> GetAllPartnersAsync();

        #endregion

        #region Update

        Task<Partner> UpdatePartnerAsync(Partner partner);

        #endregion

        #region Delete

        Task<Partner> DeletePartnerAsync(int id);

        #endregion
    }
}
