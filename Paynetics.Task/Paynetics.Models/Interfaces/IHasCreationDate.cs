﻿using System;

namespace Paynetics.Models.Interfaces
{
    public interface IHasCreationDate
    {
        DateTime CreateDate { get; set; }
    }
}
