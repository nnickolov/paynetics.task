﻿using Paynetics.Models.Abstraction;
using Paynetics.Models.Enums;
using System.Xml.Serialization;

namespace Paynetics.Models.Models
{
    public class Amount : BaseModelClass
    {
        public Direction Direction { get; set; }

        [XmlElement("Value")]
        public decimal Value { get; set; }

        [XmlElement("Currency")]
        public string Currency { get; set; }

        public virtual Transaction Transaction { get; set; }
    }
}
