﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace Paynetics.Models.Models
{
    public class Operation 
    {
        [XmlArray("Transactions")]
        [XmlArrayItem("Transaction")]
        public List<Transaction> Transactions { get; set; }
    }
}