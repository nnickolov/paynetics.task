﻿using System;
using System.Collections.Generic;

using Paynetics.Models.Abstraction;

namespace Paynetics.Models.Models
{
    public class Merchant : BaseModelClass
    {
        public Merchant()
        {
            this.Transactions = new List<Transaction>();
        }

        public string Name { get; set; }

        public DateTime BoardingDate { get; set; }

        public string Url { get; set; }

        public string Country { get; set; }

        public string FirstAddress { get; set; }

        public string SecondAddress { get; set; }

        public List<Transaction> Transactions { get; set; }

        public virtual Partner Partner { get; set; }
    }
}
