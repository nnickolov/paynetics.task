﻿using System.Collections.Generic;

using Paynetics.Models.Abstraction;

namespace Paynetics.Models.Models
{
    public class Partner : BaseModelClass
    {
        public Partner()
        {
            this.Merchants = new List<Merchant>();
        }

        public string Name { get; set; }

        public List<Merchant> Merchants { get; set; }
    }
}
