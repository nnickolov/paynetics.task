﻿using Paynetics.Models.Enums;
using Paynetics.Models.Abstraction;
using System.Collections.Generic;
using System.Xml.Serialization;
using System;

namespace Paynetics.Models.Models
{
    [Serializable]
    public class Transaction : BaseModelClass
    {
        [XmlElement("ExternalId")]
        public string ExternalId { get; set; }

        [XmlElement("Amount")]
        public Amount Amount { get; set; }

        public int? AmountId { get; set; }

        [XmlElement("Status")]
        public int Status { get; set; }

        [XmlElement("Debtor")]
        public Debtor Debtor { get; set; }

        public int? DebtorId { get; set; }

        [XmlElement("Beneficiary")]
        public Beneficiary Beneficiary{ get; set; }

        public int? BeneficiaryId { get; set; }

        public virtual Merchant Merchant { get; set; }
    }
}
