﻿using Paynetics.Models.Abstraction;
using System.Xml.Serialization;

namespace Paynetics.Models.Models
{
    public class Beneficiary : BaseModelClass
    {
        [XmlElement("BankName")]
        public string BankName { get; set; }
        [XmlElement("BIC")]
        public string BIC { get; set; }
        [XmlElement("IBAN")]
        public string IBAN { get; set; }
        public virtual Transaction Transaction { get; set; }
    }
}
