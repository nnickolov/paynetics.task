﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Paynetics.Models.Interfaces;

namespace Paynetics.Models.Abstraction
{
    public abstract class BaseModelClass : IHasCreationDate
    {
        [Key]
        public int Id { get; set; }

        public DateTime CreateDate { get; set; }
        
        public DateTime? DeletedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public bool IsDeleted { get; set; }
    }
}
