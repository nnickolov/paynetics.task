# Paynetics.TASK

Welcome to Paynetics Task!

SUMMARY:

For the performance of the task I used layered architecture style which is one of the most common architectural styles. I started to develop the task from the web project, then I created the models - Partner, Merchant, Transaction by task description. I had to change Transaction properties, because they were not matched with XML File. After that I created the Database project and later on Service project. Note that I am using: "Server=.\\SQLEXPRESS;Database=PayneticsDatabase;Trusted_Connection=True;MultipleActiveResultSets=true" in appsettings.json and using Database project to create Migrations and Update-Database. After updating Database with the new entities, I had created the CRUD operations for each entity, whom are very basic. My main focus was on the second part of the task where I had to Upload XML structured file and save the information in the database. By requirments every transaction had to be added to the correct merchant, but by the present structure there was no MERCHANT corresponding to the currenct transaction. I had noticed that I had to create few more entities like Amount, Debtor and Beneficiary, whow are successfuly mapped to the corresponding Transaction and saved in Database. The second part of the second part was to have a date-picker and after chosing a date to download .csv file with information about Transactions for the current period. Note that the date which have to be choosen is "2023-02-22". I had a lot of problems here because I tried at first to write a AJAX with the picked date which was blocking the file downloading (AJAX is in XML/Index.cshtml). After 10 hours of debbuging I managed to pick the date without AJAX and successfuly download the .csv file with the right information.
I will attach the xml file which I was using during the implementation of the task.

.NET DEVELOPER TASK

The task is to develop REST API writen in C#. The application should provide a possibility for users to do CRUD
operations on partners, merchants, transactions. One partner should have a id, name and merchants. One merchant should
have id, name, boarding date, url, country, address_1, address_2, and transactions . A transaction should have an id, create
date, direction ( Debit, Credit ), amount, currency, debtor IBAN, beneficiary IBAN, status if it’s successful or not and external id.
Write an endpoint that accepts a xml structured file (structure provided with in task page 2) with transactions. The
endpoint should insert the transactions to the correct merchant. Also write an endpoint that accepts a date with that date takes
the corresponding balance of each merchant for the date, creates a csv structured report as follows: Date, PartnerId,
PartnerName, MerchantId, MerchantName, Balance to date, DebitAmount for the date, CreditAmount for the date and returns it
as a file.

Note: Debits represent money being paid out of a particular account. Credits represent money being paid in.

&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;
&lt;Operation&gt;
    &lt;FileDate&gt;
        2023-02-22
    &lt;/FileDate&gt;
    &lt;Transactions&gt;
        &lt;Transaction&gt;
            &lt;ExternalId&gt;123213123123&lt;/ExternalId&gt;
            &lt;CreateDate&gt;2023-02-22T07:08:59.679Z&lt;/CreateDate&gt;
            &lt;Amount&gt;
                &lt;Direction&gt;D&lt;/Direction&gt;
                &lt;Value&gt;111.11&lt;/Value&gt;
                &lt;Currency&gt;EUR&lt;/Currency&gt;
            &lt;/Amount&gt;
            &lt;Status&gt;1&lt;/Status&gt;
            &lt;Debtor&gt;
                &lt;BankName&gt;ING BANK N.V.&lt;/BankName&gt;
                &lt;BIC&gt;INGBNL2A&lt;/BIC&gt;
                &lt;IBAN&gt;NL68INGB5831335380&lt;/IBAN&gt;
            &lt;/Debtor&gt;
            &lt;Beneficiary&gt;
                &lt;BankName&gt;Bulgarian Bank&lt;/BankName&gt;
                &lt;BIC&gt;INGBNL2A&lt;/BIC&gt;
                &lt;IBAN&gt;BG83IORT80949736921315&lt;/IBAN&gt;
            &lt;/Beneficiary&gt;
        &lt;/Transaction&gt;
        &lt;Transaction&gt;
            &lt;ExternalId&gt;123213234123&lt;/ExternalId&gt;
            &lt;CreateDate&gt;2023-02-22T07:07:44.123Z&lt;/CreateDate&gt;
            &lt;Amount&gt;
                &lt;Direction&gt;C&lt;/Direction&gt;
                &lt;Value&gt;1231.13&lt;/Value&gt;
                &lt;Currency&gt;EUR&lt;/Currency&gt;
            &lt;/Amount&gt;
            &lt;Status&gt;0&lt;/Status&gt;
            &lt;Debtor&gt;
                &lt;BankName&gt;ING BANK N.V.&lt;/BankName&gt;
                &lt;BIC&gt;INGBNL2A&lt;/BIC&gt;
                &lt;IBAN&gt;NL68INGB5831335380&lt;/IBAN&gt;
            &lt;/Debtor&gt;
            &lt;Beneficiary&gt;
                &lt;BankName&gt;Bulgarian Bank&lt;/BankName&gt;
                &lt;BIC&gt;INBGNL1B&lt;/BIC&gt;
                &lt;IBAN&gt;BG90RZBB91552112199351&lt;/IBAN&gt;
            &lt;/Beneficiary&gt;
        &lt;/Transaction&gt;
    &lt;/Transactions&gt;
&lt;/Operation&gt;